package io.fsarradin.actor4fun.examples

import io.fsarradin.actor4fun.examples.utils.{NoopActor, OutActor}
import io.fsarradin.actor4fun.{Actor, ActorRef, ActorSystem, Shutdownable}

import java.time.LocalDateTime
import scala.util.Random

// MESSAGE

enum PingPongMessage:
  case Ball
  case Missed

import io.fsarradin.actor4fun.examples.PingPongMessage.*

// PING PONG ACTOR

class PingPong(name: String, out: ActorRef, system: ActorSystem)
    extends Actor:
  val delay    = 500
  val maxCount = 10
  var count    = 0

  override def onStart(using self: ActorRef): Unit =
    println(s"${LocalDateTime.now()} $name ready")

  override def receive(sender: ActorRef)(using self: ActorRef): Effect =
    case Ball =>
      println(s"${LocalDateTime.now()} $name: received Ball")
      sendBall(sender)
    case Missed =>
      println(s"${LocalDateTime.now()} $name: received Missed")
      system.shutdown()

  private def sendBall(sender: ActorRef)(using self: ActorRef): Unit =
    Thread.sleep(delay)
    if (count < maxCount || Random.nextInt(100) >= 10)
      count += 1
      out ! name
      sender ! Ball
    else
      println(s"${LocalDateTime.now()} $name missed")
      out ! Missed
      sender ! Missed
      self ! Missed

// ==================== LOCAL ====================

@main
def run_pingpong_local(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createLocal("system")

  val out  = system.spawn("out", new NoopActor)
  val ping = system.spawn("ping", new PingPong("Ping", out, system))
  val pong = system.spawn("pong", new PingPong("Pong", out, system))

  ping.receiveFrom(pong, Ball)

  system.awaitTermination()

// ==================== REMOTE ====================

@main
def remote_out_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createRemote("system", "localhost", 18070)

  system.spawn("out", new OutActor)

  system.awaitTermination()

@main
def remote_ping_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createRemote("system", "localhost", 18080)

  val out = system.findActorForName("actor://localhost:18070/out").get
  system.spawn("ping", new PingPong("Ping", out, system))

  system.awaitTermination()

@main
def remote_pong_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createRemote("system", "localhost", 18081)

  val out = system.findActorForName("actor://localhost:18070/out").get
  system.spawn("pong", new PingPong("Pong", out, system))

  system.awaitTermination()

@main
def remote_launcher_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createRemote("system", "localhost", 18082)

  val ping = system.findActorForName("actor://localhost:18080/ping").get
  val pong = system.findActorForName("actor://localhost:18081/pong").get

  ping.receiveFrom(pong, Ball)

  Thread.sleep(500)
  system.shutdown()
  system.awaitTermination()

// WEB

object WebPingPong:
  import org.eclipse.jetty.websocket.api.*
  import org.eclipse.jetty.websocket.api.annotations.*
  import spark.Spark.*

  @WebSocket
  class PingPongServer { self =>
    private var session: Session = null

    @OnWebSocketConnect
    def connected(session: Session): Unit =
      self.session = session

    @OnWebSocketClose
    def closed(session: Session, statusCode: Int, reason: String): Unit =
      self.session = null
      println(s"websocket closed: $reason ($statusCode)")

    def send(message: Any): Unit =
      if (session != null)
        session.getRemote.sendString(message.toString)
        
    def sendPing(): Unit =
      if (session != null)
        session.getRemote.sendPing(null)
  }

  class WebActor(server: PingPongServer) extends Actor:
    override def receive(sender: ActorRef)(using self: ActorRef): Effect =
      case message =>
        println(s"${LocalDateTime.now()} web actor: send to websocket message $message")
        server.send(message)

  @main
  def run_pingpong(): Unit =
    val webServerPort = 18090
    val server = new PingPongServer
    
    import scala.concurrent.ExecutionContext.Implicits.given
    val system = ActorSystem.createRemote("web-system", "localhost", 18070)
    
    system.schedule(30000, 30000) { () =>
      println(s"send websocket ping")
      server.sendPing()
    }

    system.spawn("out", new WebActor(server))

    port(webServerPort)
    webSocket("/ws", server)
    
    println(s"Web server ready on port $webServerPort")
    init()
    system.awaitTermination()
