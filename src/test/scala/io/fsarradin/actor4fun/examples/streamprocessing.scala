package io.fsarradin.actor4fun.examples

import io.fsarradin.actor4fun.{Actor, ActorRef, ActorSystem}

import java.time.LocalDateTime
import scala.collection.mutable

object Entity:
  type Product = String
  type OrderId = String

  enum OrderStatus:
    case Captured, Prepared, Delivered, Cancelled

  enum Message:
    case Stock(
        product: Product,
        timestamp: LocalDateTime,
        quantity: Int
    )

    case Order(
        orderId: OrderId,
        timestamp: LocalDateTime,
        items: List[OrderedItem],
        status: OrderStatus
    )

    case Projection(
        product: Product,
        timestamp: LocalDateTime,
        quantity: Int
    )

  case class OrderedItem(
      product: Product,
      quantity: Int
  )

  case class OrderedQuantity(
      orderId: OrderId,
      timestamp: LocalDateTime,
      quantity: Int
  )

  enum DBCommand:
    case PutProjection(product: Product, projection: Message.Projection)
    case GetProjection(product: Product)
    case Response(product: Product, projection: Message.Projection)

import io.fsarradin.actor4fun.examples.Entity.*

object Actors:
  class Process(dbActor: ActorRef) extends Actor:
    val stocks: mutable.Map[Product, Message.Stock] = mutable.Map()

    val orderedProducts
        : mutable.Map[Product, mutable.Map[OrderId, OrderedQuantity]] =
      mutable.Map().withDefault(_ => mutable.Map())

    // diff operation
    private def computeSellableQuantityFor(product: String): Int =
      val stock: Message.Stock = stocks(product)
      val orders: mutable.Map[OrderId, OrderedQuantity] =
        orderedProducts(product)
      val orderedQuantity =
        orders
          .map(_._2.quantity)
          .sum

      stock.quantity - orderedQuantity

    private def updateOrders(
        orders: List[OrderedQuantity],
        product: Product,
        newOrder: Message.Order
    ): Unit =
      newOrder.status match
        case OrderStatus.Cancelled | OrderStatus.Delivered =>
          val update = orderedProducts(product).subtractOne(newOrder.orderId)
          if (update.isEmpty)
            orderedProducts.remove(product)
          else
            orderedProducts.update(product, update)
        case _ =>
          orderedProducts.update(
            product,
            orderedProducts(product).addAll(orders.map(newOrder.orderId -> _))
          )

    override def receive(sender: ActorRef)(using self: ActorRef): Effect =
      case m @ Message.Stock(product, timestamp, quantity) =>
        println(s"Get message: $m")
        stocks.update(product, Message.Stock(product, timestamp, quantity))
        val sellableQuantity = computeSellableQuantityFor(product)
        val projection: Message.Projection =
          Message.Projection(
            product = product,
            timestamp = LocalDateTime.now(),
            quantity = sellableQuantity
          )
        dbActor ! DBCommand.PutProjection(product, projection)

      case m @ Message.Order(orderId, timestamp, items, status) =>
        println(s"Get message: $m")
        val boughtItems: Map[String, List[OrderedQuantity]] =
          items.groupMap(_.product)(item =>
            OrderedQuantity(orderId, timestamp, item.quantity)
          )
        for {
          (product, orders) <- boughtItems
        } {
          updateOrders(orders, product, m)

          if (stocks.contains(product)) {
            val sellableQuantity = computeSellableQuantityFor(product)
            val projection: Message.Projection =
              Message.Projection(
                product = product,
                timestamp = LocalDateTime.now(),
                quantity = sellableQuantity
              )
            dbActor ! DBCommand.PutProjection(product, projection)
          }
        }

  class Db extends Actor:
    import DBCommand.*

    val projections: mutable.Map[String, Message.Projection] = mutable.Map()

    override def receive(sender: ActorRef)(using self: ActorRef): Effect =
      case PutProjection(product, projection) =>
        projections.update(product, projection)

      case GetProjection(product) =>
        sender ! Response(product, projections(product))

  class DbClient(dbActor: ActorRef) extends Actor:
    import DBCommand.*

    override def receive(sender: ActorRef)(using self: ActorRef): Effect =
      case m @ GetProjection(_) =>
        println(s"Sending $m to DB...")
        dbActor ! m

      case Response(product, data: Message.Projection) =>
        println(s"Stored projection: $data")

@main
def test_local_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createLocal("system")

  val dbActor      = system.spawn("db", new Actors.Db)
  val processActor = system.spawn("process", new Actors.Process(dbActor))
  val clientActor  = system.spawn("client", new Actors.DbClient(dbActor))

  processActor.send(
    Message.Stock(
      product = "banana",
      timestamp = LocalDateTime.now(),
      quantity = 100
    )
  )

  processActor.send(
    Message.Stock(
      product = "chocolate",
      timestamp = LocalDateTime.now(),
      quantity = 50
    )
  )

  processActor.send(
    Message.Order(
      orderId = "123",
      timestamp = LocalDateTime.now(),
      items = List(
        OrderedItem(product = "banana", quantity = 10),
        OrderedItem(product = "chocolate", quantity = 5)
      ),
      status = OrderStatus.Captured
    )
  )
  processActor.send(
    Message.Order(
      orderId = "456",
      timestamp = LocalDateTime.now(),
      items = List(
        OrderedItem(product = "banana", quantity = 12)
      ),
      status = OrderStatus.Prepared
    )
  )

  Thread.sleep(1000)

  clientActor.send(DBCommand.GetProjection("banana"))
  clientActor.send(DBCommand.GetProjection("chocolate"))

  processActor.send(
    Message.Order(
      orderId = "456",
      timestamp = LocalDateTime.now(),
      items = List(
        OrderedItem(product = "banana", quantity = 12)
      ),
      status = OrderStatus.Cancelled
    )
  )
  Thread.sleep(1000)
  clientActor.send(DBCommand.GetProjection("banana"))
  Thread()
  system.shutdown()
  system.awaitTermination()

object Config:
  val host        = "locahost"
  val processPort = 108080
  val dbPort      = 108081

  val processAddress = s"actor://$host:$processPort/process"
  val dbAddress      = s"actor://$host:$dbPort/db"

@main
def remote_db_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createRemote("db-system", Config.host, Config.dbPort)

  system.spawn("db", new Actors.Db)

  system.awaitTermination()

@main
def remote_process_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system =
    ActorSystem.createRemote("process-system", Config.host, Config.processPort)

  val dbActor = system.findActorForName(Config.dbAddress).get
  system.spawn("process", new Actors.Process(dbActor))

  system.awaitTermination()

@main
def send_stock_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createRemote("system", "localhost", 18090)

  val dbActor      = system.findActorForName(Config.dbAddress).get
  val processActor = system.findActorForName(Config.processAddress).get

  println("Send stock...")

  processActor.send(
    Message.Stock(
      product = "banana",
      timestamp = LocalDateTime.now(),
      quantity = 100
    )
  )

  processActor.send(
    Message.Stock(
      product = "chocolate",
      timestamp = LocalDateTime.now(),
      quantity = 50
    )
  )

  Thread.sleep(1000)

  println("Show stored projection")

  dbActor.send(DBCommand.GetProjection("banana"))
  dbActor.send(DBCommand.GetProjection("chocolate"))

  Thread.sleep(500)
  system.shutdown()
  system.awaitTermination()

@main
def send_order_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createRemote("system", "localhost", 18091)

  val dbActor      = system.findActorForName(Config.dbAddress).get
  val processActor = system.findActorForName(Config.processAddress).get

  println("Send orders...")

  processActor.send(
    Message.Order(
      orderId = "123",
      timestamp = LocalDateTime.now(),
      items = List(
        OrderedItem(product = "banana", quantity = 10),
        OrderedItem(product = "chocolate", quantity = 5)
      ),
      status = OrderStatus.Captured
    )
  )
  processActor.send(
    Message.Order(
      orderId = "456",
      timestamp = LocalDateTime.now(),
      items = List(
        OrderedItem(product = "banana", quantity = 12)
      ),
      status = OrderStatus.Prepared
    )
  )

  Thread.sleep(1000)

  println("Show stored projection")

  dbActor.send(DBCommand.GetProjection("banana"))
  dbActor.send(DBCommand.GetProjection("chocolate"))

  Thread.sleep(500)
  system.shutdown()
  system.awaitTermination()
