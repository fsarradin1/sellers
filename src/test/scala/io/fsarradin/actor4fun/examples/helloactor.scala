package io.fsarradin.actor4fun.examples

import io.fsarradin.actor4fun.{Actor, ActorRef, ActorSystem}

case class Hello(name: String)
case class Bye(name: String)

class HelloActor extends Actor:
  override def receive(sender: ActorRef)(using self: ActorRef): Effect =
    case Hello(name) => println(s"Hello $name!")
    case Bye(name) => println(s"Bye $name!")


@main
def run_hello_local(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createLocal("system")

  val actor = system.spawn("hello", new HelloActor)

  actor.send(Hello("jon"))
  actor.send(Bye("jon"))

  system.shutdown()
  system.awaitTermination()

