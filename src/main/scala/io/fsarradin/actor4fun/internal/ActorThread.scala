package io.fsarradin.actor4fun.internal

import io.fsarradin.actor4fun.{Actor, ActorRef, Shutdownable}

import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.{BlockingQueue, LinkedBlockingQueue, TimeUnit}
import scala.annotation.tailrec


private[internal] class ActorThread(
    name: String,
    actor: Actor
) extends Shutdownable:
  val inbox: BlockingQueue[ActorMessage] = new LinkedBlockingQueue()
  val isRunning: AtomicBoolean           = new AtomicBoolean(false)

  def pushMessage(message: ActorMessage): Unit =
    inbox.put(message)

  @tailrec
  private def loop(self: ActorRef): Unit =
    if (isRunning.get())
      Option(inbox.poll(1000, TimeUnit.MILLISECONDS))
        .foreach(message => processMessage(self, message))
      /* Here, we use a recursive function.
       * The tailrec annotation above ensures that we have a tail
       * recursive function, that is then translated by Scala compiler
       * into a while-loop in the bytecode. So, at the end, we have no
       * recursive call and we have no StackOverflowError.
       */
      loop(self)
    else ()

  private def processMessage(
      self: ActorRef,
      actorMessage: ActorMessage
  ): Unit =
    given ActorRef = self
    actor.receive(actorMessage.sender)(actorMessage.message)

  def start(self: ActorRef): Unit =
    if (isRunning.compareAndSet(false, true))
      given ActorRef = self
      actor.onStart
      loop(self)

  override def shutdown(): Unit =
    if (isRunning.compareAndSet(true, false))
      actor.onShutdown()
