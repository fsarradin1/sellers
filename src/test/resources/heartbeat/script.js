const driver = document.getElementById('driver');
const rightContainer = document.getElementById('rightContainer');
const ballContainer = document.getElementById('balls');
const out = document.getElementById('output');

const moveTime = 1000

const ws = new WebSocket("ws://localhost:18090/ws")

let executors = {
    'elements': [],
    'circles': {},
    'balls': {}
};

function createCircle(name) {
    const circle = document.createElement('div');
    circle.classList.add('right');
    circle.id = 'circle-' + name;
    circle.appendChild(document.createTextNode(name))
    rightContainer.appendChild(circle);

    return circle;
}

function createBall(name) {
    const ball = document.createElement('div');
    ball.classList.add('ball');
    ball.id = 'ball-' + name;
    ballContainer.appendChild(ball);

    return ball;
}

function addExecutor(name) {
    let circle = createCircle(name)
    executors.elements.push(name);
    executors.circles[name] = circle;
    executors.balls[name] = createBall(name);
}

function removeExecutor(name) {
    let index = executors.elements.indexOf(name);
    if (index > -1) {
        executors.elements.splice(index, 1);
        
        let circle = executors.circles[name];
        delete executors.circles[name];
        rightContainer.removeChild(circle);
        
        let ball = executors.balls[name];
        delete executors.balls[name];
        ballContainer.removeChild(ball);
    }
}

function redrawScene() {
    let step = Math.round(100.0 / (executors.elements.length + 1));

    for (let i = 0; i < executors.elements.length; i++) {
        let name = executors.elements[i];
        executors.circles[name].style.top = step * (i + 1) + '%';
    }
}

ws.onmessage = (event) => {
    let data = JSON.parse(event.data);
    if (data['operation'] === 'add') {
        let name = data['id'];
        if (name === 'driver') {
            driver.style.display = 'flex';
        } else {
            addExecutor(name);
        }
        redrawScene();
    }
    else if (data['operation'] === 'remove') {
        let name = data['id'];
        if (name === 'driver') {
            driver.style.display = 'none';
        } else {
            removeExecutor(name);
        }
        redrawScene();
    }
    else if (data['operation'] === 'move') {
        let targetName = data['target'];
        let fromName = data['from'];

        let ball;
        let target;
        if (targetName === 'driver') {
            target = driver;
        } else {
            target = executors.circles[targetName];
            ball = executors.balls[targetName];
        }

        let from;
        if (fromName === 'driver') {
            from = driver;
        } else {
            from = executors.circles[fromName];
            ball = executors.balls[fromName];
        }

        ball.style.backgroundColor = data['color'];

        animateMoveTo(from, target, ball, moveTime);
    }
}

function animateMoveTo(from, target, ball, duration) {
    const targetPosition = target.getBoundingClientRect();
    const fromPosition = from.getBoundingClientRect();
    const targetTop = targetPosition.top + targetPosition.height / 2;
    const fromTop = fromPosition.top + fromPosition.height / 2;

    const startTime = performance.now();

    function animate(currentTime) {
        const t = (currentTime - startTime) / duration;
        if (t < 1) {
            let left = fromPosition.left + (targetPosition.left - fromPosition.left) * t;
            let top = fromTop + (targetTop - fromTop) * t;

            ball.style.left = `${left}px`;
            ball.style.top = `${top}px`;
            requestAnimationFrame(animate);
        } else {
            moveBallTo(from, target, ball);
        }
    }

    requestAnimationFrame(animate);
}

function moveBallTo(from, target, ball) {
    const targetPosition = target.getBoundingClientRect();
    // const fromPosition = from.getBoundingClientRect();
    const ballPosition = ball.getBoundingClientRect();

    ball.style.left = `${targetPosition.left - ballPosition.width}px`;
}
