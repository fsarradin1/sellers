package io.fsarradin.actor4fun

trait Shutdownable:
  def shutdown(): Unit