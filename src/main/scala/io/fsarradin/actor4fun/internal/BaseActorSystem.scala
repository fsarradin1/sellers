package io.fsarradin.actor4fun.internal

import io.fsarradin.actor4fun.{Actor, ActorRef, ActorSystem, ScheduleTask}

import java.util.{Timer, TimerTask}
import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

private[internal] abstract class BaseActorSystem(val name: String)(using
    ExecutionContext
) extends ActorSystem:

  val actors: mutable.Map[ActorRef, ActorThread]          = mutable.Map()
  val threads: mutable.Map[ActorRef, Future[ActorThread]] = mutable.Map()
  val refs: mutable.Map[String, ActorRef]                 = mutable.Map()
  val timer: Timer = new Timer(s"$name-timer")

  override val actorNames: Set[String] = refs.keySet.toSet

  override def findActorForName(name: String): Option[ActorRef] =
    refs.get(name)

  override def unregisterAndStop(actorRef: ActorRef): Unit =
    val actor = actors(actorRef)
    actor.shutdown()
    actors.remove(actorRef)
    threads.remove(actorRef)
    refs.remove(actorRef.name)

  override def clearActors(): Unit =
    actors.values.foreach(_.shutdown())
    actors.clear()
    threads.clear()
    refs.clear()

  /** @inheritdoc */
  override def schedule(delay: Int, period: Int)(
      task: () => Unit
  ): ScheduleTask =
    val timerTask: TimerTask = () => task()
    timer.schedule(timerTask, delay, period)
    new ScheduleTask(timerTask)

  /** @inheritdoc */
  override def scheduleOnce(delay: Int)(task: () => Unit): ScheduleTask =
    val timerTask: TimerTask = () => task()
    timer.schedule(timerTask, delay)
    new ScheduleTask(timerTask)
