package io.fsarradin.actor4fun.examples

import io.fsarradin.actor4fun.{Actor, ActorRef, ActorSystem}

import java.time.LocalDateTime
import scala.collection.mutable
import scala.util.control.NonFatal

enum HbMessage:
  case Register(id: String)
  case RegisterAgain(id: String)
  case RegisterOk(id: String)
  case AlreadyRegistered(id: String)

  case Heartbeat(id: String)
  case HeartbeatOk(id: String)

  case PoisonPill

class Driver(system: ActorSystem, out: ActorRef) extends Actor:
  import HbMessage.*

  val heartbeatTimeout = 5000

  val executors: mutable.Map[String, ActorRef]  = mutable.Map()
  val heartbeatTimes: mutable.Map[String, Long] = mutable.Map()

  override def onStart(using self: ActorRef): Unit =
    out ! s"""{"operation":"add","id":"driver"}"""
    system.schedule(100, heartbeatTimeout) { () =>
      println(
        s"${LocalDateTime.now()}: Driver - check executors: ${executors.keySet.mkString(", ")}"
      )
      val now = System.nanoTime()
      for ((id, time) <- heartbeatTimes)
        if ((now - time) > heartbeatTimeout * 1_000_000L)
          println(
            s"${LocalDateTime.now()}: Driver - executor $id took too many time to send heartbeat (unregistering it)"
          )
          executors.subtractOne(id)
          heartbeatTimes.subtractOne(id)
    }

  override def receive(sender: ActorRef)(using self: ActorRef): Effect =
    case Register(id) =>
      if (executors.contains(id)) sender ! AlreadyRegistered(id)
      else
        executors.update(id, sender)
        heartbeatTimes.update(id, System.nanoTime())
        Thread.sleep(1000)
        out ! s"""{"operation":"move","from":"driver","target":"$id","color":"green"}"""
        sender ! RegisterOk(id)
        println(s"${LocalDateTime.now()}: Driver - registered executor $id")

    case Heartbeat(id) =>
      if (executors.contains(id))
        println(s"${LocalDateTime.now()}: Driver - heartbeat from executor $id")
        heartbeatTimes.update(id, System.nanoTime())
        Thread.sleep(1000)
        out ! s"""{"operation":"move","from":"driver","target":"$id","color":"blue"}"""
        sender ! HeartbeatOk(id)
      else
        out ! s"""{"operation":"move","from":"driver","target":"$id","color":"black"}"""
        sender ! RegisterAgain(id)

  override def onShutdown(): Unit =
    out.send(s"""{"operation":"remove","id":"driver"}""")

class Executor(id: String, driver: ActorRef, system: ActorSystem, out: ActorRef)
    extends Actor:
  current =>
  import HbMessage.*

  val heartbeatPeriod  = 5000
  val heartbeatTimeout = 3000

  var lastSentHeartbeatTime: Long = 0L

  private def sendHeartbeat(using self: ActorRef): Unit =
    println(
      s"${LocalDateTime.now()}: Executor ${current.id} - send heartbeat to driver"
    )
    try {
      out ! s"""{"operation":"move","from":"$id","target":"driver","color":"red"}"""
      driver ! Heartbeat(current.id)
      lastSentHeartbeatTime = System.nanoTime()
      system.scheduleOnce(heartbeatTimeout) { () =>
        if (
          System
            .nanoTime() - lastSentHeartbeatTime >= heartbeatTimeout * 1_000_000L
        )
          println(
            s"${LocalDateTime.now()}: Executor ${current.id} - waited too long for driver to answer heartbeat"
          )
          self ! PoisonPill
      }
    } catch {
      case NonFatal(e) =>
        println(
          s"${LocalDateTime.now()}: Executor ${current.id} - driver absent"
        )
        self ! PoisonPill
    }

  override def onStart(using self: ActorRef): Unit =
    out ! s"""{"operation":"add","id":"$id"}"""
    println(
      s"${LocalDateTime.now()}: Executor ${current.id} - asks driver to resgister"
    )
    out ! s"""{"operation":"move","from":"$id","target":"driver","color":"black"}"""
    driver ! Register(id)

  override def receive(sender: ActorRef)(using self: ActorRef): Effect =
    case RegisterOk(id) =>
      println(s"${LocalDateTime.now()}: Executor ${current.id} registered")
      system.schedule(1000, heartbeatPeriod)(() => sendHeartbeat)

    case RegisterAgain(id) =>
      println(
        s"${LocalDateTime.now()}: Executor ${current.id} - driver asks to register again"
      )
      out ! s"""{"operation":"move","from":"$id","target":"driver","color":"black"}"""
      driver ! Register(current.id)

    case HeartbeatOk(id) =>
      val diff = System.nanoTime() - lastSentHeartbeatTime
      println(
        s"${LocalDateTime.now()}: Executor ${current.id} - got heartbeat response in ${diff / 1e6}ms"
      )
      lastSentHeartbeatTime = System.nanoTime() + 3L * 1_000_000_000L

    case PoisonPill =>
      system.shutdown()

  override def onShutdown(): Unit =
    out.send(s"""{"operation":"remove","id":"$id"}""")

@main
def heartbeat_driver_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createRemote("driver-system", "localhost", 18080)
  sys.addShutdownHook(system.shutdown())

  val out = system.findActorForName("actor://localhost:18070/out").get
  system.spawn("driver", new Driver(system, out))

  system.awaitTermination()

def heartbeat_executor_run(id: Int): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val name = s"E$id"
  val system =
    ActorSystem.createRemote(s"$name-system", "localhost", 18080 + id)
  sys.addShutdownHook(system.shutdown())

  val out    = system.findActorForName("actor://localhost:18070/out").get
  val driver = system.findActorForName("actor://localhost:18080/driver").get
  system.spawn(name, new Executor(name, driver, system, out))

  system.awaitTermination()

@main def executor_1_run(): Unit = heartbeat_executor_run(1)
@main def executor_2_run(): Unit = heartbeat_executor_run(2)
@main def executor_3_run(): Unit = heartbeat_executor_run(3)

object WebHeartbeat:
  import org.eclipse.jetty.websocket.api.*
  import org.eclipse.jetty.websocket.api.annotations.*
  import spark.Spark.*

  @WebSocket
  class HeartbeatHandler { self =>
    private var session: Session = null

    @OnWebSocketConnect
    def connected(session: Session): Unit =
      self.session = session

    @OnWebSocketClose
    def closed(session: Session, statusCode: Int, reason: String): Unit =
      self.session = null
      println(s"websocket closed: $reason ($statusCode)")

    def send(message: Any): Unit =
      if (session != null)
        session.getRemote.sendString(message.toString)

    def sendPing(): Unit =
      if (session != null)
        session.getRemote.sendPing(null)
  }

  class WebActor(handler: HeartbeatHandler) extends Actor:
    override def receive(sender: ActorRef)(using self: ActorRef): Effect =
      case message =>
        println(
          s"${LocalDateTime.now()} web actor: send to websocket message $message"
        )
        handler.send(message)

  @main
  def run_heartbeat(): Unit =
    import scala.concurrent.ExecutionContext.Implicits.given

    val webServerPort = 18090
    val handler       = new HeartbeatHandler

    val system = ActorSystem.createRemote("system-web", "localhost", 18070)

    system.schedule(30000, 30000) { () =>
      println(s"send websocket ping")
      handler.sendPing()
    }

    system.spawn("out", new WebActor(handler))

    port(webServerPort)
    webSocket("/ws", handler)

    println(s"Web server ready on port $webServerPort")
    init()
    system.awaitTermination()
