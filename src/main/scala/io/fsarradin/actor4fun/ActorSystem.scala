package io.fsarradin.actor4fun

import io.fsarradin.actor4fun.internal.{LocalActorSystem, RemoteActorSystem}

import java.util.TimerTask
import scala.concurrent.ExecutionContext

/** Execution context for actors
  */
trait ActorSystem extends Shutdownable:

  /** Register an actor with its name and manage its communication.
    *
    * @param actor
    *   actor to register
    * @param name
    *   name under which the actor should be registered
    * @return
    *   a reference to the registered actor
    */
  def spawn(name: String, actor: Actor): ActorRef

  /** Get set of registered actor names.
    *
    * This method is specifically used to send the registered actors by one
    * actor system to a remote one.
    *
    * @return
    *   set of registered actor names.
    */
  val actorNames: Set[String]

  /** Retrieve an actor reference by its name.
    *
    * @param name
    *   actor to retrieve
    * @return
    *   the actor reference or None if the name is unknown.
    */
  def findActorForName(name: String): Option[ActorRef]

  /** Unregister and stop an actor.
    *
    * @param actorRef
    *   reference of the actor to unregister.
    */
  def unregisterAndStop(actorRef: ActorRef): Unit

  /** Shutdown and remove all actors from the register.
    */
  def clearActors(): Unit

  /** Shutdown this actor system and all its registered actors.
    */
  def shutdown(): Unit

  /** Wait for actor system thread to stop.
    */
  def awaitTermination(): Unit

  /** Schedule repetitive tasks.
    *
    * @param delay
    *   time to wait before starting the first task (in milliseconds).
    * @param period
    *   time to wait between the end of a task and its following starts (in
    *   milliseconds).
    * @param task
    *   the task to schedule.
    * @return
    *   a reference the task that can be used to cancel it.
    */
  def schedule(delay: Int, period: Int)(task: () => Unit): ScheduleTask

  /** Schedule one shot task.
    *
    * @param delay
    *   time to wait before starting the first task (in milliseconds).
    * @param task
    *   the task to schedule.
    * @return
    *   a reference the task that can be used to cancel it.
    */
  def scheduleOnce(delay: Int)(task: () => Unit): ScheduleTask

object ActorSystem:
  def createLocal(name: String)(using ExecutionContext): LocalActorSystem =
    new LocalActorSystem(name)

  def createRemote(
      name: String,
      host: String,
      port: Int
  )(using ExecutionContext): RemoteActorSystem =
    new RemoteActorSystem(name, host, port)

class ScheduleTask private[actor4fun] (timerTask: TimerTask)
    extends Shutdownable:
  override def shutdown(): Unit = timerTask.cancel()
