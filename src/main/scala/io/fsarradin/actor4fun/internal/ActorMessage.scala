package io.fsarradin.actor4fun.internal

import io.fsarradin.actor4fun.ActorRef

/** Internal representation of a message sent between actors.
  *
  * @param sender
  *   sender of the message.
  * @param message
  *   content.
  */
case class ActorMessage(sender: ActorRef, message: Any)
