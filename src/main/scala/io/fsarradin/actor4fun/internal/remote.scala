package io.fsarradin.actor4fun.internal

import actor4fun.internal.message.*
import actor4fun.internal.message.ActorEndPointGrpc.ActorEndPoint
import com.google.protobuf.ByteString
import com.google.protobuf.empty.Empty
import io.fsarradin.actor4fun.{Actor, ActorRef, ActorSystem}
import io.grpc.{ManagedChannel, ManagedChannelBuilder, Server, ServerBuilder}

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}
import java.net.{InetAddress, URI}
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try

case class RemoteActorRef(
    override val name: String,
    remoteRef: RemoteActorSystemRef,
    system: RemoteActorSystem
) extends ActorRef:

  val isLocal: Boolean  = remoteRef == system.self
  val isRemote: Boolean = !isLocal

  override def receiveFrom(sender: ActorRef, message: Any): Unit =
    this match
      case ref @ RemoteActorRef(name, _, system) if ref.isLocal =>
        localSend(sender, message, name, system)

      case ref @ RemoteActorRef(_, receiverSysRef, _) if ref.isRemote =>
        sender match
          case RemoteActorRef(_, senderSysRef, system) =>
            remoteSendFrom(
              sender,
              message,
              receiverSysRef,
              senderSysRef,
              system
            )

          case ActorRef.Null =>
            remoteSendFrom(
              ActorRef.Null,
              message,
              receiverSysRef,
              system.self,
              system
            )

          case _ =>
            throw new IllegalArgumentException(
              s"unmanaged sender reference $sender"
            )

      case _ =>
        throw new IllegalArgumentException(
          s"unmanaged receiver reference $this"
        )

  private def localSend(
      sender: ActorRef,
      message: Any,
      name: String,
      system: RemoteActorSystem
  ): Unit =
    system
      .actors(system.refs(name))
      .pushMessage(ActorMessage(sender, message))

  private def remoteSendFrom(
      sender: ActorRef,
      message: Any,
      receiverSysRef: RemoteActorSystemRef,
      senderSysRef: RemoteActorSystemRef,
      system: RemoteActorSystem
  ): Unit =
    val senderRef =
      NetActorRef(
        senderSysRef.host,
        senderSysRef.port,
        sender.name
      )

    val receiverRef =
      NetActorRef(
        receiverSysRef.host,
        receiverSysRef.port,
        this.name
      )

    val channel =
      system.connect(receiverSysRef.host, receiverSysRef.port)
    try {
      val payload = ByteString.copyFrom(RemoteActorSystem.serialize(message))
      val ack =
        ActorEndPointGrpc
          .blockingStub(channel)
          .receive(
            NetActorMessage(
              sender = Option(senderRef),
              receiver = Option(receiverRef),
              payload = payload
            )
          )

      if (!ack.isOk)
        throw new IllegalArgumentException(
          s"error from remote actor system: ${ack.error}"
        )
    } finally {
      channel.shutdown()
    }

object RemoteActorRef:
  val uriActorScheme = "actor"

  def fromURI(uriStr: String, system: ActorSystem): Option[RemoteActorRef] =
    system match
      case _: LocalActorSystem => None
      case remoteSys: RemoteActorSystem =>
        for ((name, sysRef) <- fromURI(uriStr))
          yield RemoteActorRef(name, sysRef, remoteSys)

  private[internal] def fromURI(
      uriStr: String
  ): Option[(String, RemoteActorSystemRef)] =
    for {
      uri    <- Try { URI.create(uriStr) }.toOption
      scheme <- Option(uri.getScheme)
      if scheme == uriActorScheme
      host <- Option(uri.getHost)
      port <- Option(uri.getPort).filter(_ >= 0)
      path <- Option(uri.getPath)
    } yield {
      val name = if (path.startsWith("/")) path.tail else path
      (name, RemoteActorSystemRef(host, port))
    }

case class RemoteActorSystemRef(host: String, port: Int)

class RemoteActorSystem(
    override val name: String,
    val host: String,
    val port: Int
)(using ExecutionContext)
    extends BaseActorSystem(name):
  system =>
  val address: String = InetAddress.getByName(host).getHostAddress

  val self: RemoteActorSystemRef = RemoteActorSystemRef(address, port)

  private val server: Server = createServer(port)

  locally {
    println(s"starting server on $port...")
    server.start()
    println(s"server started on $port")
  }

  private def createServer(port: Int): Server =
    val service =
      ActorEndPointGrpc.bindService(
        new RemoteHandlerService,
        ExecutionContext.global
      )

    val server = ServerBuilder.forPort(port)
    server.addService(service)

    server.build()

  override def spawn(name: String, actor: Actor): ActorRef =
    if (refs.contains(name))
      throw new IllegalArgumentException(s"$name already registered")
    else
      val actorThread: ActorThread = new ActorThread(name, actor)
      val remoteRef: ActorRef      = RemoteActorRef(name, self, system)
      actors.update(remoteRef, actorThread)
      refs.update(remoteRef.name, remoteRef)
      threads.update(
        remoteRef,
        Future {
          actorThread.start(remoteRef); actorThread
        }
      )
      remoteRef

  override def findActorForName(name: String): Option[ActorRef] =
    if (name.startsWith(s"${RemoteActorRef.uriActorScheme}://"))
      val ref = RemoteActorRef.fromURI(name)
      val local =
        ref
          .filter { case (_, sysRef) => sysRef == self }
          .flatMap { case (name, _) => refs.get(name) }

      local.orElse {
        ref.flatMap { case (name, sysRef) =>
          val names = getRemoteNames(sysRef.host, sysRef.port)
          if (names.contains(name))
            Some(RemoteActorRef(name, sysRef, system))
          else
            None
        }
      }
    else refs.get(name)

  private def getRemoteNames(host: String, port: Int): Set[String] =
    val channel: ManagedChannel = connect(host, port)
    try {
      ActorEndPointGrpc
        .blockingStub(channel)
        .actorNames(Empty())
        .names
        .toSet
    } finally {
      channel.shutdown()
    }

  private[internal] def connect(host: String, port: Int): ManagedChannel =
    val builder = ManagedChannelBuilder.forAddress(host, port)
    builder.usePlaintext()
    builder.build()

  override def shutdown(): Unit =
    timer.cancel()
    server.shutdown()
    val future = Future.sequence(threads.values.toList)
    actors.values.foreach(_.shutdown())
    Await.ready(
      future,
      Duration(1, "second")
    )

  override def awaitTermination(): Unit =
    val future = Future.sequence(threads.values.toList)
    server.awaitTermination()
    Await.ready(future, Duration.Inf)

  class RemoteHandlerService extends ActorEndPoint:
    def toRemoteActorRef(netRef: NetActorRef): RemoteActorRef =
      RemoteActorRef(
        netRef.name,
        RemoteActorSystemRef(
          host = InetAddress.getByName(netRef.host).getHostAddress,
          port = netRef.port
        ),
        system
      )

    override def receive(request: NetActorMessage): Future[Ack] =
      val response: Option[Ack] =
        for {
          receiver <- request.receiver.map(toRemoteActorRef)
          sender   <- request.sender.map(toRemoteActorRef)
        } yield sendRequest(receiver, sender, request)

      response.fold {
        val errorMessage =
          s"$name(${self.host}:${self.port}): receiver or sender is missing"

        // TODO find better way to report this error
        Future.successful(Ack(isOk = false, error = errorMessage))
      } { r =>
        Future.successful(r)
      }

    private def sendRequest(
        receiver: RemoteActorRef,
        sender: RemoteActorRef,
        request: NetActorMessage
    ): Ack =
      if (receiver.remoteRef != self)
        // receiver actor system is not the current one
        val errorMessage =
          s"$name(${self.host}:${self.port}): this system is not the target of ${receiver.remoteRef.host}:${receiver.remoteRef.port}"

        Ack(isOk = false, error = errorMessage)
      else if (!refs.contains(receiver.name))
        // this actor systems does not know the receiver actor name
        val errorMessage =
          s"$name(${self.host}:${self.port}): unknown actor with name ${receiver.name}"

        Ack(isOk = false, error = errorMessage)
      else
        val payload = deserializePayload(request.payload)
        val target  = refs(receiver.name)
        target.receiveFrom(sender, payload)

        Ack(isOk = true)

    private def deserializePayload(payload: ByteString): Any =
      val data = Array.ofDim[Byte](payload.size())
      payload.copyTo(data, 0)
      RemoteActorSystem.deserialize(data)

    override def actorNames(request: Empty): Future[ActorNames] =
      Future {
        ActorNames(refs.keys.toSeq)
      }

object RemoteActorSystem:
  def serialize(payload: Any): Array[Byte] =
    val bos = new ByteArrayOutputStream()
    try {
      val oos = new ObjectOutputStream(bos)
      oos.writeObject(payload)

      bos.toByteArray
    } finally {
      bos.close()
    }

  def deserialize(payload: Array[Byte]): Any =
    val bis = new ByteArrayInputStream(payload)
    try {
      val ois = new ObjectInputStream(bis)
      ois.readObject()
    } finally {
      bis.close()
    }
