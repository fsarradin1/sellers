package io.fsarradin.actor4fun.internal

import io.fsarradin.actor4fun.{Actor, ActorRef}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

case class LocalActorRef(
    override val name: String,
    system: BaseActorSystem
) extends ActorRef:
  override def receiveFrom(sender: ActorRef, message: Any): Unit =
//    println(s"sending from $sender: $message")
    system.actors
      .get(this)
      .map(_.pushMessage(ActorMessage(sender, message)))
      .toRight(new IllegalStateException(s"unknown actor $name"))
      .toTry
      .get

class LocalActorSystem(
    override val name: String
)(using ExecutionContext)
    extends BaseActorSystem(name):

  override def spawn(name: String, actor: Actor): ActorRef =
    if (refs.contains(name))
      throw new IllegalArgumentException(s"$name already registered")

    val actorThread = new ActorThread(name, actor)
    val actorRef: ActorRef = LocalActorRef(name, this)

    actors.update(actorRef, actorThread)
    refs.update(actorRef.name, actorRef)
    threads.update(
      actorRef,
      Future { actorThread.start(actorRef); actorThread }
    )

    actorRef

  override def shutdown(): Unit =
    timer.cancel()
    val future = Future.sequence(threads.values.toList)
    actors.values.foreach(_.shutdown())
    Await.ready(
      future,
      Duration.apply(5, "seconds")
    )

  override def awaitTermination(): Unit =
    val future = Future.sequence(threads.values.toList)
    Await.ready(future, Duration.Inf)
