addSbtPlugin("com.github.sbt" % "sbt-dynver"   % "5.0.1")
addSbtPlugin("com.thesamet"   % "sbt-protoc"   % "1.0.7")
addSbtPlugin("org.scalameta"  % "sbt-scalafmt" % "2.5.2")

libraryDependencies += "com.thesamet.scalapb" %% "compilerplugin" % "0.11.15"
