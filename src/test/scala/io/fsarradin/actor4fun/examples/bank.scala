package io.fsarradin.actor4fun.examples

import io.fsarradin.actor4fun.internal.RemoteActorSystem
import io.fsarradin.actor4fun.{Actor, ActorRef, ActorSystem, Shutdownable}

import java.time.LocalDateTime
import java.util.UUID
import java.util.concurrent.CountDownLatch
import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future, Promise}

// MESSAGES

enum BankRequest:
  case OpenAccount(id: String, balance: Double)
  case Withdraw(id: String, amount: Double)
  case Deposit(id: String, amount: Double)
  case BalanceOf(id: String, correlationId: UUID)

enum BankResponse:
  case Response(balance: Double, correlationId: UUID)
  case Error(message: String, correlationId: UUID)

// ACTORS

class BankServiceActor extends Actor:
  val accounts: mutable.Map[String, Double] = mutable.Map()

  override def receive(sender: ActorRef)(using self: ActorRef): Effect =
    case BankRequest.OpenAccount(id, balance) =>
      println(s"${LocalDateTime.now()}: Service - open account for $id with balance of $balance")
      accounts.update(id, balance)

    case BankRequest.Deposit(id, amount) =>
      println(s"${LocalDateTime.now()}: Service - deposit for $id of $amount")
      accounts.update(id, accounts(id) + amount)

    case BankRequest.Withdraw(id, amount) =>
      println(s"${LocalDateTime.now()}: Service - withdraw for $id of $amount")
      accounts.update(id, accounts(id) - amount)

    case BankRequest.BalanceOf(id, correlationId) =>
      println(s"${LocalDateTime.now()}: Service - get balance for $id")
      if (accounts.contains(id))
        val balance = accounts(id)
        sender ! BankResponse.Response(balance, correlationId)
      else
        sender ! BankResponse.Error(s"unknown account id: $id", correlationId)

class BankProxyActor(bank: ActorRef) extends Actor:
  val latch = new CountDownLatch(1)
  var current: ActorRef = null

  val sessions: mutable.Map[UUID, Promise[Double]] = mutable.Map()

  override def onStart(using self: ActorRef): Unit =
    current = self
    latch.countDown()

  def openAccount(id: String, balance: Double): Unit =
    latch.await()
    bank.receiveFrom(current, BankRequest.OpenAccount(id, balance))

  def deposit(id: String, amount: Double): Unit =
    latch.await()
    bank.receiveFrom(current, BankRequest.Deposit(id, amount))

  def withdraw(id: String, amount: Double): Unit =
    latch.await()
    bank.receiveFrom(current, BankRequest.Withdraw(id, amount))

  def getBalanceOf(id: String): Double =
    val promise: Promise[Double] = Promise[Double]()
    val correlationId            = UUID.randomUUID()
    sessions.update(correlationId, promise)
    val future: Future[Double] = promise.future

    latch.await()
    current.receiveFrom(current, BankRequest.BalanceOf(id, correlationId))
    Await.result(future, Duration.Inf)

  override def receive(sender: ActorRef)(using self: ActorRef): Effect =
    case m: BankRequest =>
      bank ! m

    case BankResponse.Response(balance, correlationId) =>
      val promise = sessions(correlationId)
      sessions.subtractOne(correlationId)
      promise.success(balance)

    case BankResponse.Error(message, correlationId) =>
      val promise = sessions(correlationId)
      sessions.subtractOne(correlationId)
      promise.failure(new IllegalStateException(message))

    case m =>
      println(s"unknown message $m")

class BankConnection(localPort: Int, host: String, port: Int)(using
    ExecutionContext
) extends Shutdownable:
  val system: RemoteActorSystem =
    ActorSystem.createRemote("system", "localhost", localPort)
  val bank: ActorRef  = system.findActorForName(s"actor://$host:$port/bank").get
  val proxy = new BankProxyActor(bank)
  val proxyActor: ActorRef = system.spawn("proxy", proxy)

  def openAccount(id: String, balance: Double): Unit =
    proxy.openAccount(id, balance)

  def deposit(id: String, amount: Double): Unit =
    proxy.deposit(id, amount)

  def withdraw(id: String, amount: Double): Unit =
    proxy.withdraw(id, amount)

  def getBalanceOf(id: String): Double =
    proxy.getBalanceOf(id)

  override def shutdown(): Unit =
    system.shutdown()
    system.awaitTermination()

@main
def bank_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createLocal("system")
//  sys.addShutdownHook(system.shutdown())

  val bank  = system.spawn("bank", new BankServiceActor)
  val proxy = new BankProxyActor(bank)
  system.spawn("proxy", proxy)

  proxy.openAccount("123", 100)
  proxy.deposit("123", 23)
  proxy.withdraw("123", 81)
  println(s"balance: ${proxy.getBalanceOf("123")}")

  system.shutdown()
  system.awaitTermination()

@main
def remote_bank_run(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val system = ActorSystem.createRemote("bank-system", "localhost", 18080)
  sys.addShutdownHook(system.shutdown())

  system.spawn("bank", new BankServiceActor)

  system.awaitTermination()

@main
def remote_client(): Unit =
  import scala.concurrent.ExecutionContext.Implicits.given
  val bank = new BankConnection(18081, "localhost", 18080)

  bank.openAccount("123", 100)
  bank.deposit("123", 23)
  bank.withdraw("123", 81)
  val balance = bank.getBalanceOf("123")
  println(s"balance: $balance")

  bank.shutdown()
