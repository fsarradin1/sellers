name := "actor4fun"

scalaVersion := "3.4.0"

libraryDependencies ++= Seq(
  "io.grpc"                 % "grpc-netty"              % scalapb.compiler.Version.grpcJavaVersion,
  "com.thesamet.scalapb"   %% "scalapb-runtime-grpc"    % scalapb.compiler.Version.scalapbVersion,
  "com.thesamet.scalapb"   %% "scalapb-runtime"         % scalapb.compiler.Version.scalapbVersion % "protobuf",
  "com.sparkjava"           % "spark-core"              % "2.9.4" % Test
)

Compile / PB.targets := Seq(
  scalapb.gen() -> (Compile / sourceManaged).value / "scalapb"
)
