package io.fsarradin.actor4fun.examples

import io.fsarradin.actor4fun.{Actor, ActorRef}

import java.time.LocalDateTime

object utils:
  class NoopActor extends Actor:
    override def receive(sender: ActorRef)(using self: ActorRef): Effect =
      case _ => ()

  class OutActor extends Actor:
    override def receive(sender: ActorRef)(using self: ActorRef): Effect =
      case message =>
        println(s"${LocalDateTime.now()}: $message")

